ARG TAG=latest
FROM debian:${TAG}

RUN apt update \
 && apt upgrade -y \
 && apt install --no-install-recommends -y initramfs-tools \
 && ln -sf /bin/true /usr/sbin/update-initramfs \
 && apt install --no-install-recommends -y linux-image-generic \
 && apt install --no-install-recommends -y systemd \
 && rm -rf /var/lib/apt/lists/*

RUN cd /boot; ln -s vmlinuz* wyrcan.kernel
RUN ln -s /bin/systemd /init
